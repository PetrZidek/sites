## Cabinet_G3
``` c
cabinet.set("visible", (value(status/warningDoorOpened) == true))
```

## DoorDisplay7_G3
``` c
statusColors = [
	["white"],
	["blue"],
	["green"],
	["red"],
	["yellow"],
	["red", "yellow"],
	["red", "green"],
	["yellow", "green"],
	["red", "yellow", "green"],
	["red", "blue"],
	["green", "blue"],
	["red", "green", "blue"]
]

sc = value(status/color)

if (statusColors[sc].size() > 1) {
	number.set("blinking", statusColors[sc], 1 sec)
}
else {
	number.setColor(statusColors[sc])
}
```

## Gate_G3
### Gate
``` c
```

### GateData
``` c
requestInhibited.set("visible", (value(status/routeRequestInhibited) == true))

show data to the table in SVG in format "time, vehicleId, route, lineNumber, requestSourced"

```

## Heating_G3
``` c
if (value(status/heatingState) == HeatingState::OFF) {
  statusText.set("text", "OFF")
  statusText.set("color", "black")
}
else if (value(status/heatingState) == HeatingState::ON) {
  statusText.set("text", "Permanent ON")
  statusText.set("color", "black")
}
else if (value(status/heatingState) == HeatingState::AUTO) {
  statusText.set("text", "AUTO")
  statusText.set("color", "black")
}
else if (value(status/heatingState) == HeatingState::Unknown) {
  statusText.set("text", "Unknown")
  statusText.set("color", "black")
}
tempAir.set("text", value(tempAir))
tempMeteo.set("text", value(tempMeteo))
tempRail.set("text", value(tempRail))
if (value(wetConditions)== true)
wetConditions.set("text", "Yes")
else
wetConditions.set("text", "No")
```

## HeatingRod_G3
``` c
if (value(status/heatingOn) == true)
  rod.set("color", "red")
else
  rod.set("color", "black")
```

## MPS_G3
``` c
if (value(status/turnedOnManually) == true)
  mps.set("color", "white")
else
  mps.set("color", "gray")

rows = 3

for (i = 0; i < rows; i++)
  rowData = rows[i]

  if (rowData)
    { text, color, align, blinking } = rowData

    rowElement.set("text", text)
    if (color)
      rowElement.set("color", color)
    else
      rowElement.set("color", "black")

    rowElement.set("align", align)
    rowElement.set("blinking", blinking)
    rowElement.set("visible", true)
  else
    rowElement.set("visible", false)
```

### MatrixVertical
``` c
if (value(status/turnedOnManually) == true)
  mps.set("color", "white")
else
  mps.set("color", "gray")

rows = 3

if (rows.count() == 1)
  firstRowElement.set("color", color)
  firstRowElement.set("blinking", blinking)
  firstRowElement.set("text", text)
else
  for (i = 0; i < rows; i++)
    rowData = rows[i]

    if (rowData)
      { text, color, align, blinking } = rowData

      rowElement.set("text", text)
      if (color)
        rowElement.set("color", color)
      else
        rowElement.set("color", "black")

      if (i == 0)
         rowElement.set("align", left)
      else if (i == 1)
         rowElement.set("align", center)
      else if (i == 2)
         rowElement.set("align", right)

      rowElement.set("blinking", blinking)
      rowElement.set("visible", true)
    else
      rowElement.set("visible", false)
```

## PMM_G3
``` c
posLeft.set("visible", (value(status/posLeft) == true) && !value((status/posMiddle) == true))
posRight.set("visible", (value(status/posRight) == true) && !value((status/posMiddle) == true))
leverInSocket.set("visible", (value(status/leverInSocket) == true))
```

## PME_G3
``` c
posLeft.set("visible", (value(status/posLeft) == true) && !value((status/posMiddle) == true))
posRight.set("visible", (value(status/posRight) == true) && !value((status/posMiddle) == true))
leverInSocket.set("visible", (value(status/leverInSocket) == true))
motorMoving.set("visible", (value(status/motorMoving) == true))
```

## RequestorDigital
``` c
if (value(buttonsStatus) > 0) {
	box.set("color", "blue")
}
else if (value(status/resetPressed) == true) {
	box.set("color", "yellow")
}
else {
	box.set("color", "white")
}
```

## Route_G3
``` c
enum RouteState
{
	Free = 0,
	Build = 1,
	Ready = 3,
	Destroy = 5,
	Occupied = 7,
	Error = 15,
};

route.set("opacity", 1)
if (value(status/routeState) == RouteState::Error) {
    route.set("stroke", "red")
    route.set("visible", true)
}
else if (value(status/routeState) == RouteState::Occupied) {
    route.set("stroke", "orange")
    route.set("visible", true)
}
else if (value(status/routeState) == RouteState::Ready) {
    route.set("stroke", "#6beb34")	//green
    route.set("visible", true)
}
else if (value(status/routeState) == RouteState::Build) {
    route.set("stroke", "magenta")
    route.set("opacity", 0.7)
    route.set("visible", true)
}
else if (value(status/routeState) == RouteState::Destroy) {
    route.set("stroke", "magenta")
    route.set("opacity", 0.7)
    route.set("visible", true)
}
else if (value(status/routeState) == RouteState::Free) {
    route.set("visible", false)
}
else {
	route.set("stroke", "black")
    route.set("visible", true)
}
```

## SignalSymbol_G3
``` c
enum SymbolState {Off = 0, Shining, Blinking, Error};
switch (value(status/symbolState) {
	case SymbolState::Off:
		symbol.set("visible", false)
		symbol.set("blinking", false)
		break;
	case SymbolState::Shining:
		symbol.set("visible", true)
		symbol.set("blinking", false)
		break;
	case SymbolState::Blinking:
		symbol.set("visible", true)
		symbol.set("blinking", true)
		break;
	case SymbolState::Error:
		symbol.set("visible", false)
		symbol.set("blinking", false)
		break;
	default:
		symbol.set("visible", false)
		symbol.set("blinking", false)
		break;
	}
```

## Detectors
### Detector (TC, PD, MD, IS, VTC...)
``` c
detector.set("opacity", 0.75)

if (value(status/occupied) == true) {
  detector.set("color", "yellow")
}
else if (value(status/claimed) == true) {
  detector.set("color", "lightgreen")
}
else {
  detector.set("color", "white")
}
```

## Track
### VehicleCounter
```c
vehicleCount.set("text", value(vehicleCount))
```

## Requestors
### Requestor (AWA, VETRA, VECOM, SPIE, DRR, MPRX...)
``` c
radioBeams.set("visible", (value(status/activeTramCommunication) == true))

if (valueChange(vehicleDetected)) {
	transceiver.set("singleBlink", yellow->white)
}
```

## RequestorData
``` c
tramId.set("text", value(vehicleDetected.vehicleId))
lineNumber.set("text", value(vehicleDetected.lineNumber))
routeCode.set("text", value(vehicleDetected.routeCode))
serviceNumber.set("text", value(vehicleDetected.serviceNumber))
vehicleType.set("text", value(vehicleDetected.vehicleType))
direction.set("text", value(vehicleDetected.direction))
category.set("text", value(vehicleDetected.category))
stand.set("text", value(vehicleDetected.stand))
side.set("text", value(vehicleDetected.side))
remoteCmd.set("text", value(vehicleDetected.remoteCommand))
readyToStart.set("text", value(vehicleDetected.readyToStart))
```

## RequestorDataMel
``` c
tramId.set("text", value(vehicleDetected.vehicleId))
lineNumber.set("text", value(vehicleDetected.lineNumber))
routeCode.set("text", value(vehicleDetected.routeCode))
prime.set("text", value(vehicleDetected.routeCode.prime))
branch.set("text", value(vehicleDetected.routeCode.branch))
dir.set("text", value(vehicleDetected.routeCode.dir))
serviceNumber.set("text", value(vehicleDetected.serviceNumber))
vehicleType.set("text", value(vehicleDetected.vehicleType))
direction.set("text", value(vehicleDetected.direction))
category.set("text", value(vehicleDetected.category))
stand.set("text", value(vehicleDetected.stand))
side.set("text", value(vehicleDetected.side))
remoteCmd.set("text", value(vehicleDetected.remoteCommand))
readyToStart.set("text", value(vehicleDetected.readyToStart))
```


## Zone_G3
``` c
enum ZoneState { Unknown = 0, Normal = 1, AllBlocked = 2, EmergencyInhibit = 3 }

if (value(status/zoneState) == ZoneState::EmergencyInhibit) {
  statusText.set("text", "Emergency Inhibit")
  statusText.set("color", "red")
}
else if (value(status/zoneState) == ZoneState::AllBlocked) {
  statusText.set("text", "All Blocked")
  statusText.set("color", "#ffcc00")
}
else if (value(status/zoneState) == ZoneState::Normal) {
  statusText.set("text", "Normal")
  statusText.set("color", "black")
}
else if (value(status/zoneState) == ZoneState::Unknown) {
  statusText.set("text", "Unknown")
  statusText.set("color", "black")
}

```
## CabinetRFID_G3
``` c
userID.set("text", value(userID))
```