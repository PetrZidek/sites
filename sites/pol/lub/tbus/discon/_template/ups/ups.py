import asyncio
import logging
import time

from chainpack.rpcclient import RpcClient

logging.basicConfig(
    level=logging.DEBUG, format="%(levelname)s[%(module)s:%(lineno)d] %(message)s"
)


async def test():
    live = 0
    client = RpcClient()
    print("connecting to broker")
    await client.connect(
        host="localhost",
        password="shv3y45",
        user="admin",
        login_type=RpcClient.LoginType.Plain,
    )
    print("connected OK")

    while 1:
        await client.call_shv_method("shv/visu/devices/cabinet/ups/status", "get", 0)
        resp = await client.read_rpc_message()
        print("response received:", resp.result().value)
        if (resp.result().value & 0x1) == 1:
            print("UPS ready")
            live = 0
        if (resp.result().value & 0x1000) == 4096:
            live = live + 10
            print("UPS buffering", live)
        else:
            live = 0

        if live >= 60*60*4:
            print("Inhibit UPS")
            await client.call_shv_method("shv/visu/devices/cabinet/ups/inhibitUPS", "set", 1)
            resp = await client.read_rpc_message()
        else:
            await client.call_shv_method("shv/visu/devices/cabinet/ups/inhibitUPS", "set", 0)
            resp = await client.read_rpc_message()


        time.sleep(10)


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(test())
