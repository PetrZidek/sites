<
    "version": 3,
	"types": {
		"StatusGeneral": {
			"type": "BitField",
			"fields": [
				{"name": "batteryError", "value": 0, "description": "PLC battery is missing or low voltage"},
			]
		},
		"StatusZone": {
			"type": "BitField",
			"fields": [
				{"name": "emergencyInhibit", "value": 0, "description": "Emergency inhibit",
					"tags": {"alarm": "error", "alarmLevel": 70 }
				},
				{"name": "allBlocked", "value": 1, "description": "All blocked",
					"tags": {"alarm": "warning", "alarmLevel": 70 }
				},
			]
		},
		"EnumRouteState": {
			"type": "Enum",
			"fields": [
				{"name": "FREE", "value": 0},
				{"name": "RES0", "value": 23},
				{"name": "RES1", "value": 24},
				{"name": "OCCPD", "value": 30},
				{"name": "OCCPD_RES1N", "value": 31},
				{"name": "OCCPD_RES2N", "value": 32},
			]
		},
		"StatusRoute": {
			"type": "BitField",
			"fields": [
				{"name": "routeState", "typeName": "EnumRouteState", "value": [0,7]},
			]
		},
		"StatusPME": {
			"type": "BitField",
			"fields": [
				{"name": "posLeft", "value": 0, "description": "Position left"},
				{"name": "posRight", "value": 1, "description": "Position right"},
				{"name": "leverInSocket", "value": 5, "description": "Manual bar inserted"},
				{"name": "motorMoving", "value": 8, "description": "Motor moving"},
				{"name": "warningPosition", "value": 12, "description": "Effector warning", "tags": {"alarm": "warning"}},
				{"name": "errorPosition", "value": 15, "description": "Effector error", "tags": {"alarm": "error"}},
			]
		},
		"StatusPD": {
			"type": "BitField",
			"fields": [
				{"name": "pantographDetectedA", "value": 1, "description": "Pantograph detected on contact A"},
				{"name": "errorContactA", "value": 2, "description": "Error on pantograph A"},
				{"name": "pantographDetectedB", "value": 5, "description": "Pantograph detected on contact B"},
				{"name": "errorContactB", "value": 6, "description": "Error on pantograph B"},
				{"name": "sequenceError", "value": 8, "description": "Sequence error"},
			]
		},
		"StatusKSW": {
			"type": "BitField",
			"fields": [
				{"name": "allumage", "value": 1, "description": "On key-switch (CAPSYS loop) was set 'allumage'"},
				{"name": "rebrousment", "value": 2, "description": "On key-switch (CAPSYS loop) was set 'rebrousment'"},
				{"name": "junction", "value": 3, "description": "On key-switch (CAPSYS loop) was set 'junction'"},
				{"name": "depot", "value": 4, "description": "On key-switch (CAPSYS loop) was set 'depot'"},
				{"name": "surchiste", "value": 5, "description": "On key-switch (CAPSYS loop) was set 'surchiste'"},
				{"name": "anulate", "value": 6, "description": "On key-switch (CAPSYS loop) was set 'anulate'"},
				{"name": "manual", "value": 7, "description": "On key-switch (CAPSYS loop) was set 'manual'"},
				{"name": "non manual", "value": 8, "description": "On key-switch (CAPSYS loop) was set 'non manual'"},
				{"name": "norm", "value": 9, "description": "On key-switch (CAPSYS loop) was set 'norm'"},
				{"name": "hors service", "value": 10, "description": "On key-switch (CAPSYS loop) was set 'hors service'"},
				{"name": "", "value": 0, "description": "On key-switch (CAPSYS loop) was set ''"},
			]
		},
		"StatusControlrelay": {
			"type": "BitField",
			"fields": [
				{"name": "active", "value": 0, "description": "Active"},
				{"name": "checkIn", "value": 1, "description": "Check in"},
				{"name": "statError", "value": 15, "description": "Stat error"},
			]
		},
		"EnumSignalSymbolState": {
			"type": "Enum",
			"fields": [
				{"name": "OFF", "value": 0},
				{"name": "STOP", "value": 10},
				{"name": "PREPARE", "value": 20},
				{"name": "PREPARE2", "value": 21},
				{"name": "GO_STRAIGHT", "value": 41},
				{"name": "GO_LEFT", "value": 42},
				{"name": "GO_RIGHT", "value": 43},
				{"name": "CALIBRATION", "value": 99}
			]
		},
    "StatusSS": {
			"type": "BitField",
			"fields": [
				{"name": "symbolState", "typeName": "EnumSignalSymbolState", "label": "Symbol state", "description": "Symbol state", "value": [0,7], "tags": {"visualStyle": "symbolState"}},
				{"name": "statWarning", "value": 23, "label": "Current warning", "description": "Current is out of warning bounds (out of warning limit)", "tags": {"alarm": "warning"}},
				{"name": "statError", "value": 16, "label": "Current error", "description": "Current is out of error bounds (out of error limit) or lower than minimum current limit", "tags": {"alarm": "error"}},
			]
		},

		"StatusPPI": {
			"type": "BitField",
			"fields": [
				{"name": "firstText", "value": 1, "description": "Showing first text in Charleroi"},
				{"name": "secondText", "value": 2, "description": "Showing second text in Charleroi"},
				{"name": "thirdText", "value": 3, "description": "Showing third text in Charleroi"},
			]
		},
		"StatusHeating": {
			"type": "BitField",
			"fields": [
				{"name": "off", "value": 0, "description": "Heating off.", "tags": {}},
				{"name": "auto", "value": 1, "description": "Active automatic regulation."},
				{"name": "on", "value": 2, "description": "Active permanent heating.", "tags": {}},
				{"name": "emergency", "value": 4, "description": "Heating manually activated from cabinet"},
				{"name": "heating", "value": 8, "description": "Status heating"},
				{"name": "errorTempAir", "value": 9, "description": "Air temperature error / out of range", "tags": {"alarm": "error"}},
				{"name": "errorTempRail", "value": 10, "description": "Rail temperature error / out of range", "tags": {"alarm": "error"}},
				{"name": "errorAuto", "value": 14, "description": "Automatic regulation error, caused by air temperature error or rail temperature error", "tags": {"alarm": "error"}},
				{"name": "errorHeating", "value": 15, "description": "Heating error,  caused by rod or SHTC unit error", "tags": {"alarm": "error"}},
			]
		},
		"StatusElbox": {
			"type": "BitField",
			"fields": [
				{"name": "doorOpened", "value": 0, "description": "Cabinet door open"},
				{"name": "noVoltage", "value": 1, "description": "No voltage behind DC/DC converter", "tags": {"alarm": "error"}},
				{"name": "usingBattery", "value": 2, "description": "Power supply from batteries"},
				{"name": "upsReady", "value": 4, "description": "UPS ready"},
				{"name": "upsBuffering", "value": 5, "description": "UPS buffering"},
				{"name": "upsReplaceBattery", "value": 7, "description": "UPS replace battery"},
				{"name": "duskSensor", "value": 8, "description": "Dusk sensor"},
				{"name": "voltage24VOutOfBounds", "value": 14, "description": "Voltage 24V out of bounds"},
				{"name": "tractionVoltageOutOfBounds", "value": 15, "description": "Traction voltage out of bounds"},
			]
		},
		"StatusHeater": {
			"type": "BitField",
			"fields": [
				{"name": "heating", "value":	0, "description": "Heating ON"},
				{"name": "disabled", "value":	8, "description": "Heating rod disabled"},
				{"name": "error", "value":	15, "description": "Heating rod error", "tags": {"alarm": "error"}},
			]
		}
    }
>
{
	"PropertyRO": <
		"env": {
			"opcua.path.getter": "{{opcua.path}}.{{connector.getterNodeId?={{connector.nodeId?={{shv.nodeId}}}}}}",
		},
		"tags": {
			"monitored": true,
			"autoload": true
		},
		"methods": {
			"get": {"access": "{{getter.accessRight?=rd}}", "signature": "RetParam", "flags": ["Get"],				
				"connector": {
					"id": "{{connector.id}}",
					"path": "{{opcua.path.getter}}",
					"method": "getValue",
					"shvType": "{{connector.shvType?=}}",
					"nativeType": "{{connector.nativeType?=}}",
				}
			},
			"chng": {"access": "{{getter.accessRight?=rd}}", "signature": "VoidParam", "flags": ["Sig"] },
		}
	>{},
	
	"General": <
		"env": {
			"opcua.path": "{{opcua.modulePrefix}}.general",
		},
		"tags": {"deviceType": "General"}
	>{
		"wdtCounter_PLC": < "nodeType": "PropertyRO", "tags": {"typeName": "UInt"} >{},
		"status": < "nodeType": "PropertyRO", "tags": {"typeName": "StatusGeneral"} >{}
	},

	"PME": <
		"env": {
			"opcua.path": "{{opcua.modulePrefix}}.pme[{{opcua.ix}}].pme_dev"
		},
		"tags": {"deviceType": "PME"}
		"methods": {
		}
	>{
		"status": < "nodeType": "PropertyRO", "tags": {"typeName": "StatusPME"} >{},		
	},

	"PD": < // Pantograph Detector
		"env": {
			"opcua.path": "{{opcua.modulePrefix}}.pantograph[{{opcua.ix}}].pantograph_dev"
		},
		"tags": {"deviceType": "PD"}
	>{
		"status": < "nodeType": "PropertyRO", "tags": {"typeName": "StatusPD"} >{},
	},

	"Elbox": <
		"env": {
			"opcua.path": "{{opcua.modulePrefix}}.elbox[{{opcua.ix}}].elbox_dev"
		},
		"tags": {"deviceType": "Elbox"}
	>{
		"status": < "nodeType": "PropertyRO", "tags": {"typeName": "StatusElbox"} >{}
		"temperature": < "nodeType": "PropertyRO",
			"tags": {"monitorOptions": {"dataChangeFilter": {"deadbandValue": 0.5 } }, "typeName": "Decimal(1)", "unit": "°C", "description": "Actual cabinet temperature."},
			"env": {"connector.nodeId": "temperature.{{shv.nodeId}}", "connector.shvType": "Decimal(1)"},
		>{},
		"temperatureBottom": < "nodeType": "PropertyRO",
			"tags": {"monitorOptions": {"dataChangeFilter": {"deadbandValue": 0.5 } }, "typeName": "Decimal(1)", "unit": "°C", "description": "Actual cabinet temperature at the bottom."},
			"env": {"connector.nodeId": "temperature.temperature_bottom", "connector.shvType": "Decimal(1)"},
		>{},
	},

	"KSW": <
		"env": {
			"opcua.path": "{{opcua.modulePrefix}}.ksw[{{opcua.ix}}].ksw_dev"
		},
		"tags": {"deviceType": "KSW"},
	>{
		"status": < "nodeType": "PropertyRO", "tags": {"typeName": "StatusKSW"} >{}
	},

	"SS": <
		"env": {
			"opcua.path": "{{opcua.modulePrefix}}.ss[{{opcua.ix}}].ss_dev",
		},
		"tags": {"deviceType": "SS"}
	>{
		"status": < "nodeType": "PropertyRO", "tags": {"typeName": "StatusSS"} >{}
	},

	"Controlrelay": <
		"env": {
			"opcua.path": "{{opcua.modulePrefix}}.controlrelay[{{opcua.ix}}].controlrelay_dev",
		},
		"tags": {"deviceType": "Controlrelay"}
    >{
		"status": < "nodeType": "PropertyRO", "tags": {"typeName": "StatusControlrelay"} >{}
    }, 

	"Zone": <
		"env": {
			"opcua.path": "{{opcua.modulePrefix}}.zone[{{opcua.ix}}]",
		},
		"tags": {"deviceType": "Zone"}
	>{
		"zoneDisconnected": < "nodeType": "PropertyRO", "env": {"opcua.path": "connector", "connector.nodeId": "offline"}, "tags": {"monitored": false, "typeName": "Bool", "alarm": "error", "label": "Zone disconnected", "description": "Zone PLC is disconnected"} >{},
		"status": < "nodeType": "PropertyRO", "tags": {"typeName": "StatusZone"} >{},		
	},

	"Route": <
		"env": {
			"opcua.path": "{{opcua.modulePrefix}}.route[{{opcua.ix}}]"
		},
		"tags": {"deviceType": "Route"}
	>{
		"status": < "nodeType": "PropertyRO", "tags": {"typeName": "StatusRoute"}, "env": {"connector.shvType": "UInt"} >{}
		"warning_long_occpd": < "nodeType": "PropertyRO", "env": {"connector.shvType": "Bool"} >{}
	},

	"Heater": <
		"env": {
			"opcua.path": "{{opcua.modulePrefix}}.heaters[{{opcua.ix}}].heater_dev",
		},
		"tags": {"deviceType": "Heater"}
	>{
		"status": < "nodeType": "PropertyRO", "tags": {"typeName": "StatusHeater"}, "env": {"connector.shvType": "UInt"} >{}
	},

	"Heating": <
		"env": {
			"opcua.path": "{{opcua.modulePrefix}}.heating[{{opcua.ix}}].heating_dev",
		},
		"tags": {"deviceType": "Heating"}
	>{
		"tempAir": < "nodeType": "PropertyRO",
			"env": {"connector.nodeId": "temp_air", "connector.shvType": "Decimal(1)"},
			"tags": {"monitorOptions": {"dataChangeFilter": {"deadbandValue": 1 } }, "typeName": "Decimal(1)", "unit": "°C", "description": "Air temperature" },
		>{},
		"tempRail": < "nodeType": "PropertyRO",
			"env": {"connector.nodeId": "temp_rail", "connector.shvType": "Decimal(1)"},
			"tags": {"monitorOptions": {"dataChangeFilter": {"deadbandValue": 1 } }, "typeName": "Decimal(1)", "unit": "°C", "description": "Rail temperature" },
		>{},
		"status": < "nodeType": "PropertyRO", "tags": {"typeName": "StatusHeating"}, "env": {"connector.shvType": "UInt"} >{}
	},

	"PPI": <
		"env": {
			"opcua.path": "{{opcua.modulePrefix}}.ppi[{{opcua.ix}}].ppi_dev",
		},
		"tags": {"deviceType": "PPI"}
	>{
		"status": < "nodeType": "PropertyRO", "tags": {"typeName": "StatusPPI"} >{}
	}

}
