<
	"version": 2,
	"types":{
		"Position":{
			"fields":[
				{"name":"front", "value":1, "label": "Front", "description": "Transceiver in front of vehicle" },
				{"name":"rear", "value":2, "label": "Rear", "description": "Transceiver in back of vehicle"  }
			],
			"type":"Enum"
		},
		"EnumRouteCodeMel":{
			"fields":[
				{"name":"none", "value":0, "label":"None", "description":"None" },
				{"name":"up", "value":1, "label":"Up", "description":"Up" },
				{"name":"down", "value":2, "label":"Down", "description":"Down" },
			],
			"typeName":"Enum"
		},
		"RouteCodeMel":{
			"fields":[
				{
					"name":"prime",
					"label":"Prime",
					"description":"Prime",
					"typeName":"Int",
					"value":[6, 15]
				},
				{
					"name":"branch",
					"label":"Branch",
					"description":"Branch",
					"typeName":"Int",
					"value":[2, 5]
				},
				{
					"name":"dir",
					"label":"Direction",
					"description":"Direction",
					"typeName":"EnumRouteCodeMel",
					"value":[0, 1]
				},
			],
			"typeName":"BitField"
		},
		"TscVehicleDetected":{
			"fields":[
				{"name":"vehicleId", "typeName":"Int", "label": "Vehicle Id", "description": "ID (number) of detected vehicle" },
				{"name":"routeCode", "typeName":"RouteCodeMel", "label": "Route code", "description": "Vehicle route code"},
				{"name":"lineNumber", "typeName":"Int", "label": "Line number", "description": "Line number"},
				{"name":"vehiclePosition", "typeName":"Position", "label": "Vehicle position" },
				{"name":"transceiverPosition", "typeName":"Position", "label": "Transceiver position" },
				{"name":"direction", "typeName":"Direction", "label": "Direction" },
				{"name":"transceiverId", "typeName":"Int", "label": "Transceiver ID " },
				{"name":"destination", "typeName":"Int", "label": "Destination" }
			],
			"sampleType":"Discrete",
			"type":"Map"
		},
		"AwaVehicleDetected":{
			"fields":[
				{"name":"direction", "typeName":"Direction", "label": "Direction" },
			],
			"sampleType":"Discrete",
			"type":"Map"
		},
		"Direction":{
			"fields":[
				{"name":"unknown", "value":0, "label": "Unknown" },
				{"name":"left", "value":1, "label": "Left" },
				{"name":"right", "value":2, "label": "Right" },
				{"name":"straight", "value":3, "label": "Straight" }
			],
			"type":"Enum"
		},
		"StatusTc": {
			"type": "BitField",
			"fields": [
				{"name": "occupied", "value": 0, "label": "Occupied", "description": "Detector is currently occupied"},
				{"name": "error", "value": 15, "label": "Error", "description": "Hardware error", "tags": {"alarm": "error"}},
			]
		},
		"StatusCabinet": {
			"type": "BitField",
			"fields": [
				{"name": "warningDoorOpened", "value": 0, "label": "Cabinet door open", "description": "Cabinet door currently open", "tags": {"alarm": "warning"}},
			]
		},
		"StatusPme": {
			"type": "BitField",
			"fields": [
				{"name": "posLeft", "value": 0, "label": "Position left", "description": "Point machine is switched to the left"},
				{"name": "posRight", "value": 1, "label": "Position right", "description": "Point machine is switched to the right"},
				{"name": "leverInSocket", "value": 5, "label": "Hand-lever in socket", "description": "Lever for manual switching in socket"},
				{"name": "errorPosition", "value": 15, "label": "Position error", "description": "Effectors on both sides are active at the same time", "tags": {"alarm": "error"}},
			]
		},
		"StatusSystem": {
			"type": "BitField",
			"fields": [
				{"name": "critical", "value": 0, "label": "Critical error", "description": "Critical error in system", "tags": {"alarm": "error"}},
				{"name": "blocked", "value": 1, "label": "System blocked", "description": "System is blocked for tram passage"},
			]
		},
		"StatusPpi": {
			"type": "BitField",
			"fields": [
				{"name": "straight", "value": 1, "label": "Straight direction" },
				{"name": "left", "value": 2, "label": "Left direction" },
				{"name": "right", "value": 3, "label": "Right direction" },
				{"name": "cross", "value": 4, "label": "Cross sign" },
				{"name": "stop", "value": 5, "label": "Stop sign" },
				{"name": "redcross", "value": 6, "label": "Red cross sign" },
				{"name": "error", "value": 15, "label": "Error", "tags": {"alarm": "error"}},
			]
		},
		"StatusHeater": {
			"type": "BitField",
			"fields": [
				{"name": "heating", "value": 0, "label": "Heater is heating" },
				{"name": "error", "value": 15, "label": "Heater error", "tags": {"alarm": "error"}},
			]
		},
		"HeatingState": {
			"type": "Enum",
			"fields": [
				{ "name": "off", "value": 1, "label": "Off", "description": "Off" },
				{ "name": "auto", "value": 2, "label": "Auto", "description": "Auto" },
				{ "name": "on", "value": 4,  "label": "On", "description": "On" },
			]
		},
		"StatusHeating": {
			"type": "BitField",
			"fields": [
			    { "name": "state", "value": [0,2], "label": "Heating state", "tags": {"typeName": "HeatingState" }},
			    { "name": "emergency", "value": 4, "label": "Emergency", "tags": {"alarm": "error"} },
			    { "name": "heating", "value": 8, "label": "Heating is heating" },
			    { "name": "errorAirTemp", "value" 9: , "label": "Error of air temperature", "tags": {"alarm": "error"} },
			    { "name": "errorRailTemp", "value": 10, "label": "Error of rail temperature", "tags": {"alarm": "error"} },
			    { "name": "errorMeteoTemp", "value": 11, "label": "Error of tempearature of meteo roof", "tags": {"alarm": "error"} },
			    { "name": "errorMeteo", "value": 13, "label": "Error of meteo roof", "tags": {"alarm": "error"} },
			    { "name": "errorAuto", "value": 14, "label": "Error of automatic mode", "tags": {"alarm": "error"} },
			    { "name": "errorHeating", "value": 15: , "label": "Error of heating", "tags": {"alarm": "error"} },
			]
		},
		"UserLoginId": {
			"type": "String",
			"sampleType": "Discrete"
		},
		"SystemError": {
			"type": "String",
			"sampleType": "Discrete"
		}
	}
>
{
	"PropertyRO": <
		"tags": {
			"monitored": true,
			"autoload": true
		},
		"methods": {
			// get can have int param max_age_msec:
			// null or ommitted: DONT_CARE - value::cacheForMsec is used
			//							 -1: USE_CACHE - cached value is used
			//								0: RELOAD_FORCE - value is loaded from gate, cached value is updated
			//						n > 0: RELOAD_OLD - value is loaded from gate if it is older than n, cached value is used otherwise
			"get": {"access": "rd", "signature": "RetParam", "flags": ["Get"],
				//"typeName": "{{typeName?=}}",
				"connector": {
					"id": "{{connector.id}}",
					"method": "get",
					"path": "{{tsc.path}}"
				}
			},
			"chng": {"signature": "VoidParam", "flags": ["Sig"] },
		}
	>{},
	"PropertyRW": <
		"nodeType": "PropertyRO",
		"methods": {
			"set": {
				"access": "wr",
				"signature": "RetParam", "flags": ["Set"],
				"connector": {
					"id": "{{connector.id}}",
					"path": "{{tsc.path}}",
					"method": "set"
				}
			},
		}
	>{},
	"CABINET": <
		"tags": { "deviceType": "TscElbox" },
		"env": { "tsc.path": "shv/elbox/{{deviceName}}" },
		"methods": {}
	>{
		"userLogin": <"nodeType": "PropertyRO", "env": { "tsc.path": "{{tsc.path}}/userLogin" }, "tags": { "typeName": "UserLoginId", "label": "User login" }>{},
		"status": <"nodeType": "PropertyRO", "env": { "tsc.path": "{{tsc.path}}/status" }, "tags": { "typeName": "StatusCabinet" }>{}
	},
	"SYSTEM": <
		"tags":  {"deviceType": "TscSystem" },
		"env": { "tsc.path": "shv/system/{{deviceName}}" },
		"methods": {
			"getRoutingTable": {
				"access": "rd",
				"signature": "RetParam",
				"label": "Get routing table",
				"description": "Get routing table",
				"connector": {
					"id": "{{connector.id}}",
					"path": "",
					"method": "getRoutingTable"
				}
			},
			"setRoutingTable": {
				"access": "cmd",
				"signature": "RetParam",
				"label": "Set routing table",
				"description": "Set routing table",
				"connector": {
					"id": "{{connector.id}}",
					"path": "",
					"method": "setRoutingTable"
				}
			},
			"getCardList": {
				"access": "cmd",
				"signature": "RetParam",
				"label": "Get card list",
				"description": "Get list of installed cards",
				"connector": {
					"id": "{{connector.id}}",
					"path": "",
					"method": "getCardList"
				}
			},
			"getConfig": {
				"access": "cmd",
				"signature": "RetParam",
				"label": "Get config",
				"description": "Get device config",
				"connector": {
					"id": "{{connector.id}}",
					"path": "",
					"method": "getConfig"
				}
			},
			"setConfig": {
				"access": "cmd",
				"signature": "RetParam",
				"label": "Set config",
				"description": "Set device config",
				"connector": {
					"id": "{{connector.id}}",
					"path": "",
					"method": "setConfig"
				}
			},
		}
	>{
		"offline":<"nodeType": "PropertyRO", "env": { "tsc.path": "connector.offline" },  "tags": { "typeName": "Bool", "monitored": true, "alarm": "error", "label": "Offline", "description": "ShvGate disconnected from TSC" }>{},
		"boxTemp":<"nodeType": "PropertyRO", "env": { "tsc.path": "{{tsc.path}}/boxTemp" }, "tags": { "typeName": "Double", "label": "Box temperature", "description": "Actual temperature inside box" }>{},
		"pcbTemp":<"nodeType": "PropertyRO", "env": { "tsc.path": "{{tsc.path}}/pcbTemp" }, "tags": { "typeName": "Double", "label": "PCB temperature", "description": "Actual temparature of PCB boad" }>{},
		"voltage600":<"nodeType": "PropertyRO", "env": { "tsc.path": "{{tsc.path}}/voltage600" }, "tags": { "typeName": "Double", "label": "Voltage 600", "description": "Real value of 600V voltage" }>{},
		"error":<"nodeType": "PropertyRO", "env": { "tsc.path": "{{tsc.path}}/error" }, "tags": { "typeName": "SystemError", "label": "Last error", "description": "Latest error on TSC system" }>{},
		"ipAddress":<"nodeType": "PropertyRO", "env": { "tsc.path": "{{tsc.path}}/ipAddress" }, "tags": { "typeName": "String", "monitored": false, "label:" "IP address", "description": "IP address of TSC system" }>{},
		"deviceId":<"nodeType": "PropertyRO", "env": { "tsc.path": "{{tsc.path}}/realDeviceId" }, "tags": { "typeName": "String", "monitored": false, "label:" "Device ID", "description": "Unique ID of TSC system" }>{},
		"status":<"nodeType": "PropertyRO", "env": { "tsc.path": "{{tsc.path}}/status" },"tags": { "typeName": "StatusSystem" }>{},
	},
	"TC": <
		"tags": {"deviceType": "TscTc" },
		"methods": {}
	>{
		"status": < "nodeType": "PropertyRO", "env": { "tsc.path": "shv/tc/{{deviceName}}/status" }, "tags": {"typeName": "StatusTc"}>{}
	},
	"PPI": <
		"tags": {"deviceType": "TscPpi" },
		"methods": {}
	>{
		"status":<
			"env": { "tsc.path": "shv/ppi/{{deviceName}}/status" },
			"nodeType": "PropertyRO",
			"tags": {"typeName": "StatusPpi"},
		>{}
	},
	"PME": <
		"tags": { "deviceType": "TscPme" },
		"methods": {}
	>{
		"status":< "nodeType": "PropertyRO", "env": { "tsc.path": "shv/pme/{{deviceName}}/status" },  "tags": {"typeName": "StatusPme" }>{}
	},
	"VETRA": <
		"tags": { "deviceType": "TscVetra" },
		"methods": {}
	>{
		"vehicleDetected":<
			"env": { "tsc.path": "shv/vet/{{deviceName}}/vehicleDetected" },
			"nodeType": "PropertyRO",
			"tags": {"typeName": "TscVehicleDetected" },
		>{}
	},
	"AWA": <
		"tags": { "deviceType": "TscVetra" },
		"methods": {}
	>{
		"vehicleDetected":<
			"env": { "tsc.path": "shv/vet/{{deviceName}}/vehicleDetected" },
			"nodeType": "PropertyRO",
			"tags": {"typeName": "AwaVehicleDetected" },
		>{}
	},
	"HEATING": <
		"env": { "tsc.path": "shv/heating/{{deviceName}}" },
		"tags": { "deviceType": "TscHeating" },
		"methods": {
			"cmdOn":{
				"access": "cmd",
				"signature": "RetVoid",
				"label": "On",
				"description": "Turn heating on",
				"connector": {
					"id": "{{connector.id}}",
					"path": "{{tsc.path}}",
					"method": "cmdOn"
				}
			},
			"cmdOff":{
				"access": "cmd",
				"signature": "RetVoid",
				"label": "Off",
				"description": "Turn heating off",
				"connector": {
					"id": "{{connector.id}}",
					"path": "{{tsc.path}}",
					"method": "cmdOff"
				}
			},
			"cmdAuto":{
				"access": "cmd",
				"signature": "RetVoid",
				"label": "Auto",
				"description": "Set heating to automatic mode",
				"connector": {
					"id": "{{connector.id}}",
					"path": "{{tsc.path}}",
					"method": "cmdAuto"
				}
			}
		}
	>{
		"correction": <
			"env": { "tsc.path": "{{tsc.path}}/correction" },
			"tags": { "typeName": "Int" },
			"nodeType": "PropertyRW"
		>{},
		"hysteresis": <
			"env": { "tsc.path": "{{tsc.path}}/hysteresis" },
			"tags": { "typeName": "Int" },
			"nodeType": "PropertyRW",
		>{},
		"airTemp": <
			"env": { "tsc.path": "{{tsc.path}}/airTemp" },
			"tags": { "typeName": "Double" },
			"nodeType": "PropertyRO"
		>{},
		"railTemp": <
			"env": { "tsc.path": "{{tsc.path}}/railTemp" },
			"tags": { "typeName": "Double" },
			"nodeType": "PropertyRO"
		>{},
		"status":< 
			"env": { "tsc.path": "{{tsc.path}}/status" },  
			"tags": {"typeName": "StatusHeating" },
			"nodeType": "PropertyRO"
		>{}
	},
	"HEATER": <
		"env": { "tsc.path": "{{tsc.path}}/heater/{{heaterIndex}}" },
		"tags": { "deviceType": "TscHeater" }
	>{
		"status":< 
			"env": { "tsc.path": "{{tsc.path}}/status" },
			"tags": {"typeName": "StatusHeater" },  
			"nodeType": "PropertyRO"
		>{}
	}
}
