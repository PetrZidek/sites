<
	"version": 2
	"types":{
		"Position":{
			"fields":[
				{"name":"Front", "value":1},
				{"name":"Rear", "value":2}
			],
			"type":"Enum"
		},
		"TscVehicleDetected":{
			"fields":[
				{"name":"vehicleId", "typeName":"Int"},
				{"name":"routeCode", "typeName":"Int"},
				{"name":"lineNumber", "typeName":"Int"},
				{"name":"vehiclePosition", "typeName":"Position"},
				{"name":"transceiverPosition", "typeName":"Position"},
				{"name":"direction", "typeName":"Direction"},
				{"name":"transceiverId", "typeName":"Int"},
				{"name":"destination", "typeName":"Int"}
			],
			"sampleType":"Discrete",
			"type":"Map"
		},
		"Direction":{
			"fields":[
				{"name":"Unknown", "value":0},
				{"name":"Left", "value":1},
				{"name":"Right", "value":2},
				{"name":"Straight", "value":3}
			],
			"type":"Enum"
		}
	}
>
{
	"PropertyRO": <
		"tags": {
			"monitored": true,
			"autoload": true
		},
		"methods": {
			// get can have int param max_age_msec:
			// null or ommitted: DONT_CARE - value::cacheForMsec is used
			//							 -1: USE_CACHE - cached value is used
			//								0: RELOAD_FORCE - value is loaded from gate, cached value is updated
			//						n > 0: RELOAD_OLD - value is loaded from gate if it is older than n, cached value is used otherwise
			"get": {"access": "rd", "signature": "RetParam", "flags": ["Get"],
				//"typeName": "{{typeName?=}}",
				"connector": {
					"id": "{{connector.id}}",
					"method": "get",
					"path": "{{tsc.path}}"
				}
			},
			"chng": {"signature": "VoidParam", "flags": ["Sig"] },
		}
	>{},
	"PropertyRW": <
		"nodeType": "PropertyRO",
		"methods": {
			"set": {
				"access": "wr", 
				"signature": "RetParam", "flags": ["Set"],
				"connector": {
					"id": "{{connector.id}}",
					"path": "{{tsc.path}}",
					"method": "set"
				}
			},
		}
	>{},
	"ELBOX": <
		"tags": { "deviceType": "TscElbox" },
		"env": { "tsc.path": "shv/elbox/{{deviceName}}" },
		"methods": {}
	>{
		"userLogin": <"nodeType": "PropertyRO", "env": { "tsc.path": "{{tsc.path}}/userLogin" }, "tags": { "typeName": "String" }>{},
		"status": <
		>{
			"doorOpened":<"nodeType": "PropertyRO", "env": { "tsc.path": "{{tsc.path}}/status/doorOpened" }, "tags": { "typeName": "Bool" }>{},
		}
	},
	"SYSTEM": <
		"tags":  {"deviceType": "TscSystem" },
		"env": { "tsc.path": "shv/system/{{deviceName}}" },
		"methods": {
			"getRoutingTable": {
				"access": "rd",
				"signature": "RetParam",
				"connector": {
					"id": "{{connector.id}}",
					"path": "",
					"method": "getRoutingTable"
				}
			},
			"setRoutingTable": {
				"access": "cmd",
				"signature": "RetParam",
				"connector": {
					"id": "{{connector.id}}",
					"path": "",
					"method": "setRoutingTable"
				}
			}
		}
	>{
		"offline":<"nodeType": "PropertyRO", "env": { "tsc.path": "connector.offline" },  "tags": { "typeName": "Bool", "monitored": false, "alarm": "error", "description": "ShvGate disconnected from TSC" }>{},
		"boxTemp":<"nodeType": "PropertyRO", "env": { "tsc.path": "{{tsc.path}}/boxTemp" }, "tags": { "typeName": "Double" }>{},
		"cpuTemp":<"nodeType": "PropertyRO", "env": { "tsc.path": "{{tsc.path}}/cpuTemp" }, "tags": { "typeName": "Double" }>{},
		"pcbTemp":<"nodeType": "PropertyRO", "env": { "tsc.path": "{{tsc.path}}/pcbTemp" }, "tags": { "typeName": "Double" }>{},
		"voltage600":<"nodeType": "PropertyRO", "env": { "tsc.path": "{{tsc.path}}/voltage600" }, "tags": { "typeName": "Double" }>{},
		"error":<"nodeType": "PropertyRO", "env": { "tsc.path": "{{tsc.path}}/error" }, "tags": { "typeName": "String" }>{},
		"status":<
			"env": { "tsc.path": "{{tsc.path}}/status" }
		>{
			"blocked":<"nodeType": "PropertyRO", "env": { "tsc.path": "{{tsc.path}}/blocked" }, "tags": { "typeName": "Bool" }>{},
			"critical":<"nodeType": "PropertyRO", "env": { "tsc.path": "{{tsc.path}}/critical" }, "tags": { "typeName": "Bool" }>{},
		}
	},
	"TC": <
		"tags": {"deviceType": "TscTc" },
		"methods": {}
	>{
		"status": <
			"env": { "tsc.path": "shv/tc/{{deviceName}}/status" },
		>{
			"occupied":  <
				"env": { "tsc.path": "{{tsc.path}}/occupied" },
				"nodeType": "PropertyRO",
				"tags": {"typeName": "Bool"},
			>{},
			"error":  <
				"env": { "tsc.path": "{{tsc.path}}/error" },
				"nodeType": "PropertyRO",
				"tags": {"typeName": "Bool"},
			>{}
		}
	},
	"PPI": <
		"tags": {"deviceType": "TscPpi" },
		"methods": {}
	>{
		"status":<
			"env": { "tsc.path": "shv/ppi/{{deviceName}}/status" },
		>{
			"blocked": <
				"env": { "tsc.path": "{{tsc.path}}/blocked" },
				"nodeType": "PropertyRO",
				"tags": {"typeName": "Bool" },
			>{},
			"error": <
				"env": { "tsc.path": "{{tsc.path}}/error" },
				"nodeType": "PropertyRO",
				"tags": {"typeName": "Bool" },
			>{},
			"redCross": <
				"env": { "tsc.path": "{{tsc.path}}/redCross" },
				"nodeType": "PropertyRO",
				"tags": {"typeName": "Bool" },
			>{},
			"stop": <
				"env": { "tsc.path": "{{tsc.path}}/stop" },
				"nodeType": "PropertyRO",
				"tags": {"typeName": "Bool" },
			>{},
			"direction": <
				"env": { "tsc.path": "{{tsc.path}}/direction" },
				"nodeType": "PropertyRO",
				"tags": {"typeName": "Direction" },
			>{},
		}
	},
	"PME": <
		"tags": { "deviceType": "TscPme" },
		"methods": {}
	>{
		"status":<
			"env": { "tsc.path": "shv/pme/{{deviceName}}/status" }
		>{
			"direction": <
				"env": { "tsc.path": "{{tsc.path}}/direction" },
				"nodeType": "PropertyRO",
				"tags": {"typeName": "Direction" },
			>{},
			"errorPosition": <
				"env": { "tsc.path": "{{tsc.path}}/errorPosition" },
				"nodeType": "PropertyRO",
				"tags": {"typeName": "Bool" },
			>{},
			"manual": <
				"env": { "tsc.path": "{{tsc.path}}/manual" },
				"nodeType": "PropertyRO",
				"tags": {"typeName": "Bool" },
			>{},
			
		}
	},
	"VETRA": <
		"tags": { "deviceType": "TscVetra" },
		"methods": {}
	>{
		"vehicleDetected":<
			"env": { "tsc.path": "shv/vet/{{deviceName}}/vehicleDetected" },
				"nodeType": "PropertyRO",
				"tags": {"typeName": "TscVehicleDetected" },
		>{}
	},
	"HEATING_GROUP": <
		"env": { "tsc.path": "shv/heating" },
	>{
		"correction": <
			"env": { "tsc.path": "{{tsc.path}}/correction" },
			"tags": { "typeName": "Int" },
			"nodeType": "PropertyRW"
		>{}
	},
	"HEATING": <
		"env": { "tsc.path": "{{tsc.path}}/{{deviceName}}" },
		"tags": { "deviceType": "TscHeating" },
		"methods": {
			"cmdOn":{
				"access": "Cmd",
				"signature": "RetVoid",
				"connector": {
					"id": "{{connector.id}}",
					"path": "{{tsc.path}}",
					"method": "cmdOn"
				}
			},
			"cmdOff":{
				"access": "Cmd",
				"signature": "RetVoid",
				"connector": {
					"id": "{{connector.id}}",
					"path": "{{tsc.path}}",
					"method": "cmdOff"
				}
			},
			"cmdAuto":{
				"access": "Cmd",
				"signature": "RetVoid",
				"connector": {
					"id": "{{connector.id}}",
					"path": "{{tsc.path}}",
					"method": "cmdAuto"
				}
			}
		}
	>{
		"airTemp": <
			"env": { "tsc.path": "{{tsc.path}}/airTemp" },
			"tags": { "typeName": "Double" },
			"nodeType": "PropertyRO"
		>{},
		"railTemp": <
			"env": { "tsc.path": "{{tsc.path}}/railTemp" },
			"tags": { "typeName": "Double" },
			"nodeType": "PropertyRO"
		>{},
		"status": <
			"env": { "tsc.path": "{{tsc.path}}/status" },
		>{
			"emergency": <
				"env": { "tsc.path": "{{tsc.path}}/emergency" },
				"tags": { "typeName": "Bool" },
				"nodeType": "PropertyRO"
			>{},
			"errorAirTemp": <
				"env": { "tsc.path": "{{tsc.path}}/errorAirTemp" },
				"tags": { "typeName": "Bool" },
				"nodeType": "PropertyRO"
			>{},
			"errorAuto": <
				"env": { "tsc.path": "{{tsc.path}}/errorAuto" },
				"tags": { "typeName": "Bool" },
				"nodeType": "PropertyRO"
			>{},
			"errorHeating": <
				"env": { "tsc.path": "{{tsc.path}}/errorHeating" },
				"tags": { "typeName": "Bool" },
				"nodeType": "PropertyRO"
			>{},
			"errorMeteo": <
				"env": { "tsc.path": "{{tsc.path}}/errorMeteo" },
				"tags": { "typeName": "Bool" },
				"nodeType": "PropertyRO"
			>{},
			"errorMeteoTemp": <
				"env": { "tsc.path": "{{tsc.path}}/errorMeteoTemp" },
				"tags": { "typeName": "Bool" },
				"nodeType": "PropertyRO"
			>{},
			"errorRailTemp": <
				"env": { "tsc.path": "{{tsc.path}}/errorRailTemp" },
				"tags": { "typeName": "Bool" },
				"nodeType": "PropertyRO"
			>{},
			"heating": <
				"env": { "tsc.path": "{{tsc.path}}/heating" },
				"tags": { "typeName": "Bool" },
				"nodeType": "PropertyRO"
			>{},
			"state": <
				"env": { "tsc.path": "{{tsc.path}}/state" },
				"tags": { "typeName": "Int" },
				"nodeType": "PropertyRO"
			>{}
		}
	},
	"HEATER": <
		"env": { "tsc.path": "{{tsc.path}}/heater/{{heaterIndex}}" },
		"tags": { "deviceType": "TscHeater" }
	>{
		"status":<
			"env": { "tsc.path": "{{tsc.path}}/status" },
		>{
			"error":<
				"env": { "tsc.path": "{{tsc.path}}/error" },
				"tags": { "typeName": "Bool" },
				"nodeType": "PropertyRO"
			>{},
			"heating":<
				"env": { "tsc.path": "{{tsc.path}}/heating" },
				"tags": { "typeName": "Bool" },
				"nodeType": "PropertyRO"
			>{}
		}
	}
}
