local bot = require('telegram_bot').new('5223967314:AAGmFqV6jpYkydZh_aNb2zf8jRJ4JvBRvS4')

local function ends_with(str, ending)
	return ending == "" or str:sub(-#ending) == ending
end

local function ends_with(str, ending)
	return ending == "" or str:sub(-#ending) == ending
end

shv.on_broker_connected(function ()
	local last_severity = {}
	shv.rpc_call(".broker/currentClient", "mountPoint", "", function (value)
		shv.subscribe(value.value .. "/" .. cur_shv_path, "chng", function (path, new_value)
			if not shv.hscope_initializing() and ends_with(path, '/status') and not ends_with(path, 'agent_online/status') then
				if last_severity[path] and new_value.value.severity.value ~= last_severity[path] then
					local msg =
						new_value.value.severity.value == 'ok' and 'Test was fixed.' or 'Test failure occurred.'

					chat_ids = {
						5362232370,
						144823520,
						1303910583
					}

					for _, id in ipairs(chat_ids) do
						bot:sendMessage(id, string.format('%s\nPath: %s\nSeverity: %s\nMessage: %s', msg, path, new_value.value.severity.value, new_value.value.message.value))
					end
				end

				last_severity[path] = new_value.value.severity.value
			end
		end)
	end)
end)

